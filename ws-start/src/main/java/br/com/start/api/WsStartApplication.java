package br.com.start.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsStartApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsStartApplication.class, args);
	}

}
